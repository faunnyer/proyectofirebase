import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClienteModel } from 'src/app/model/cliente.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formulario-cliente',
  templateUrl: './formulario-cliente.component.html',
  styleUrls: ['./formulario-cliente.component.css']
})
export class FormularioClienteComponent implements OnInit {

  form: FormGroup;
  clienteEditar = null;
  accion = null;

  constructor(
    public dialogRef: MatDialogRef<FormularioClienteComponent>,
    public fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: ClienteModel
  ) {

    this.clienteEditar = data;

  }

  ngOnInit(): void {
    if (this.clienteEditar) {
      this.accion = 'Editar';
      this.form = this.fb.group({
        cedula: [this.clienteEditar.cedula, Validators.required],
        nombre: [this.clienteEditar.nombre, Validators.required],
        apellido: [this.clienteEditar.apellido, Validators.required],
        edad: [this.clienteEditar.edad, Validators.required]
      });
    } else {
      this.accion = 'Crear';
      this.form = this.fb.group({
        cedula: [null, Validators.required],
        nombre: [null, Validators.required],
        apellido: [null, Validators.required],
        edad: [null, Validators.required]
      });
    }
  }


  cancelar(): void {
    this.dialogRef.close();
  }

  guardarCliente() {
    if (this.form.valid) {
      if (this.clienteEditar) {
        const clienteEdit: ClienteModel = {
          apellido: this.form.get('apellido').value,
          cedula: this.form.get('cedula').value,
          edad: this.form.get('edad').value,
          nombre: this.form.get('nombre').value,
        };
        this.dialogRef.close({ cliente: clienteEdit, id: this.clienteEditar.id });
      } else {
        const movimiento: ClienteModel = {
          apellido: this.form.get('apellido').value,
          cedula: this.form.get('cedula').value,
          edad: this.form.get('edad').value,
          nombre: this.form.get('nombre').value,
        };
        this.dialogRef.close(movimiento);
      }
    } else {
      this.mensajeAlerta('Un momento', 'Ingrese todos los campos obligatorios', 'warning');
    }
  }


  mensajeAlerta(titulo, mensaje, icono) {
    Swal.fire({
      title: titulo,
      text: mensaje,
      icon: icono,
      confirmButtonText: 'Aceptar',
      confirmButtonColor: '#0066b3',
      cancelButtonColor: '#d33',
    });
  }


}
