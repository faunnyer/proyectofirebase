import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClienteModel } from 'src/app/model/cliente.model';

@Component({
  selector: 'app-visualizacion-cliente',
  templateUrl: './visualizacion-cliente.component.html',
  styleUrls: ['./visualizacion-cliente.component.css']
})
export class VisualizacionClienteComponent implements OnInit {


  cliente: ClienteModel = null;

  constructor(
    public dialogRef: MatDialogRef<VisualizacionClienteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ClienteModel
  ) {

    this.cliente = data;

  }

  ngOnInit(): void {
  }

  cancelar(): void {
    this.dialogRef.close();
  }

}
