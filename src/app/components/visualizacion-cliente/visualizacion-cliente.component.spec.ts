import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacionClienteComponent } from './visualizacion-cliente.component';

describe('VisualizacionClienteComponent', () => {
  let component: VisualizacionClienteComponent;
  let fixture: ComponentFixture<VisualizacionClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacionClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacionClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
