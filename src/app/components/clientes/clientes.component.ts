import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClienteModel } from 'src/app/model/cliente.model';
import { FormularioClienteComponent } from '../formulario-cliente/formulario-cliente.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { VisualizacionClienteComponent } from '../visualizacion-cliente/visualizacion-cliente.component';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnDestroy {

  displayedColumns: string[] = ['menu', 'cedula', 'nombre', 'apellido', 'edad'];
  dataSource: MatTableDataSource<ClienteModel>;
  cliente: ClienteModel;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public listaFirebaseSubscription: Subscription[] = [];

  constructor(
    public clientesService: ClientesService,
    public dialog: MatDialog
  ) {
    this.consultarClientes();
  }

  ngOnDestroy(): void {
    this.listaFirebaseSubscription.forEach(element => {
      element.unsubscribe();
    });
  }

  consultarClientes() {
    this.listaFirebaseSubscription.push(this.clientesService.consultarClientes().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => {
      console.log(error);
    }));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  abrirModalCliente(cliente) {
    const dialogRef = this.dialog.open(FormularioClienteComponent, {
      width: '500px',
      data: cliente
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.cliente = result;
        const id = result.id;
        if (id) {
          this.actualizarCliente(id, result.cliente);
        } else {
          this.guardarCliente(this.cliente);
        }
      }
    });
  }

  guardarCliente(cliente: ClienteModel) {
    this.clientesService.crearCliente(cliente).then(data => {
      console.log('cliente creado exitosamente');
      this.mensajeAlerta('Muy bien ¡¡¡', 'El cliente se ha creado exitosamente', 'success');
    }).catch(error => {
      console.error(error);
    });
  }

  actualizarCliente(id, cliente: ClienteModel) {
    this.clientesService.actualizarCliente(id, cliente).then(data => {
      console.log('cliente ACTUALIZADO exitosamente');
      this.mensajeAlerta('Muy bien ¡¡¡', 'El cliente se ha actualizado exitosamente', 'success');
    }).catch(error => {
      console.error(error);
    });
  }

  eliminarCliente(cliente: ClienteModel) {
    this.clientesService.eliminarCliente(cliente).then(data => {
      console.log('cliente ELIMINADO exitosamente');
      this.mensajeAlerta('Muy bien ¡¡¡', 'El cliente se ha eliminado exitosamente', 'success');
    }).catch(error => {
      console.error(error);
    });
  }


  mensajeAlerta(titulo, mensaje, icono) {
    Swal.fire({
      title: titulo,
      text: mensaje,
      icon: icono,
      confirmButtonText: 'Aceptar',
      confirmButtonColor: '#0066b3',
      cancelButtonColor: '#d33',
    });
  }


  abrirModalVisualizacion(cliente) {
    const dialogRef = this.dialog.open(VisualizacionClienteComponent, {
      width: '600px',
      data: cliente
    });
  }



}


