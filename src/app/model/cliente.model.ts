export class ClienteModel {
    id?: any;
    apellido: string;
    cedula: string;
    edad: number;
    nombre: string;
}
