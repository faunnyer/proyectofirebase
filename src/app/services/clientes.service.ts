import { Injectable } from '@angular/core';
import { ClienteModel } from '../model/cliente.model';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { NOMBRE_TABLAS } from '../constants/nombreTablas.constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {


  public listaClienteCollection: AngularFirestoreCollection<ClienteModel>;
  private clienteEditDoc: AngularFirestoreDocument<ClienteModel>;
  private clienteDeleteDoc: AngularFirestoreDocument<ClienteModel>;


  constructor(
    public afs: AngularFirestore
  ) {
    this.listaClienteCollection = this.afs.collection<any>(
      NOMBRE_TABLAS.TABLA_CLIENTES
    );
  }

  consultarClientes() {
    return this.listaClienteCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as any;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  crearCliente(questionData) {
    return this.afs.collection(NOMBRE_TABLAS.TABLA_CLIENTES).add(questionData);
  }

  actualizarCliente(id, cliente: ClienteModel) {
    this.clienteEditDoc = this.afs.doc<ClienteModel>(`${NOMBRE_TABLAS.TABLA_CLIENTES}/${id}`);
    return this.clienteEditDoc.update(cliente);
  }

  eliminarCliente(cliente: ClienteModel) {
    this.clienteDeleteDoc = this.afs.doc<ClienteModel>(`${NOMBRE_TABLAS.TABLA_CLIENTES}/${cliente.id}`);
    return this.clienteDeleteDoc.delete();
  }


}
